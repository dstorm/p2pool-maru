#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdio.h>
#include <math.h> 

static const int64_t COIN = 100000000;

double ConvertBitsToDouble(unsigned int nBits)
{
    int nShift = (nBits >> 24) & 0xff;

    double dDiff =
        (double)0x0000ffff / (double)(nBits & 0x00ffffff);

    while (nShift < 29)
    {
        dDiff *= 256.0;
        nShift++;
    }
    while (nShift > 29)
    {
        dDiff /= 256.0;
        nShift--;
    }

    return dDiff;
}

int64_t static GetBlockBaseValue(int nBits, int nHeight)
{
    double dDiff =
        (double)0x0000ffff / (double)(nBits & 0x00ffffff);

    dDiff = ConvertBitsToDouble(nBits);

    int64_t nSubsidy = 0;
    if(nHeight >= 2000) {
        if((nHeight >= 7000 && dDiff > 75) || nHeight >= 15000) { // GPU/ASIC difficulty calc
            // 2222222/(((x+2600)/9)^2)
            nSubsidy = (44444444.0 / (pow((dDiff+2600.0)/9.0,2.0)));
            if (nSubsidy > 25) nSubsidy = 55;
            if (nSubsidy < 5) nSubsidy = 15;
        } else { // CPU mining calc
            nSubsidy = (22222.0 / (pow((dDiff+51.0)/6.0,2.0)));
            if (nSubsidy > 500) nSubsidy = 500;
            if (nSubsidy < 25) nSubsidy = 25;
        }
    } else {
        nSubsidy = (2222.0 / (pow((dDiff+1.0),2.0)));
        if (nSubsidy > 500) nSubsidy = 500;
        if (nSubsidy < 1) nSubsidy = 1;
    }

    // printf("height %u diff %4.2f reward %i \n", nHeight, dDiff, nSubsidy);
    nSubsidy *= COIN;

    // yearly decline of production by 7% per year, projected 21.3M coins max by year 2050.
    for(int i = 210240; i <= nHeight; i += 210240) nSubsidy *= 0.93;

    return nSubsidy;
}


#include <boost/python/module.hpp>
#include <boost/python/def.hpp>
using namespace boost::python;
 
BOOST_PYTHON_MODULE(marucoin_subsidy)
{
    def("GetBlockBaseValue", GetBlockBaseValue);
}

