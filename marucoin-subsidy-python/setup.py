from distutils.core import setup
from distutils.extension import Extension

setup(name="marucoin_subsidys",
    ext_modules=[
        Extension("marucoin_subsidy", ["marucoin_GetBlockBaseValue.cpp"],
        libraries = ["boost_python"])
    ])
