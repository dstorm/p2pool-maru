import os
import platform

from twisted.internet import defer

from . import data
from p2pool.util import math, pack, jsonrpc

@defer.inlineCallbacks
def check_genesis_block(bitcoind, genesis_block_hash):
    try:
        yield bitcoind.rpc_getblock(genesis_block_hash)
    except jsonrpc.Error_for_code(-5):
        defer.returnValue(False)
    else:
        defer.returnValue(True)

nets = dict(
    marucoin=math.Object(
        P2P_PREFIX='fcc1b7de'.decode('hex'),
        P2P_PORT=7744,
        ADDRESS_VERSION=64,
        RPC_PORT=7743,
        RPC_CHECK=defer.inlineCallbacks(lambda bitcoind: defer.returnValue(
            'marucoinaddress' in (yield bitcoind.rpc_help()) and
            not (yield bitcoind.rpc_getinfo())['testnet']
        )),
        SUBSIDY_FUNC=lambda nBits, height: __import__('marucoin_subsidy').GetBlockBaseValue(nBits, height),
        BLOCKHASH_FUNC=lambda data: pack.IntType(256).unpack(__import__('x13_hash').getPoWHash(data)),
        POW_FUNC=lambda data: pack.IntType(256).unpack(__import__('x13_hash').getPoWHash(data)),
        BLOCK_PERIOD=180, # s
        SYMBOL='MARU',
        CONF_FILE_FUNC=lambda: os.path.join(os.path.join(os.environ['APPDATA'], 'Marucoin') if platform.system() == 'Windows' else os.path.expanduser('~/Library/Application Support/Marucoin/') if platform.system() == 'Darwin' else os.path.expanduser('~/.marucoin'), 'marucoin.conf'),
        BLOCK_EXPLORER_URL_PREFIX='http://cryptexplorer.com/block/',
        ADDRESS_EXPLORER_URL_PREFIX='http://cryptexplorer.com/address/',
        TX_EXPLORER_URL_PREFIX='http://cryptexplorer.com/tx/',
        SANE_TARGET_RANGE=(2**256//2**32//1000 - 1, 2**256//2**20 - 1),
        DUMB_SCRYPT_DIFF=1,
        DUST_THRESHOLD=0.001e8,
        CHARITY_ADDRESS='a2cccadeafb9da926daaa7e9b90260eafa0b102b'.decode('hex')
    ),
)
for net_name, net in nets.iteritems():
    net.NAME = net_name
